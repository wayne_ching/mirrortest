# FROM node:12.16.3-alpine3.11
# 設定工作資料夾
# WORKDIR /dist
# 新增環境變數路徑到 $PATH
# ENV TZ=Asia/Taipei
# COPY . /dist
# 安裝http-server
# RUN npm install http-server
# 執行並給port號
# CMD npx http-server ./BPMS2 -p 4200


FROM nginx:1.15.2-alpine

COPY /BPMS2/. /usr/share/nginx/html
COPY /angular.conf /etc/nginx/conf.d/angular.conf
ENV TZ=Asia/Taipei
EXPOSE 4200

ENTRYPOINT ["nginx", "-g", "daemon off;"]
